import firebase from "firebase";

var firebaseConfig = {
    apiKey: "AIzaSyBpJLMZKLeh_fKP7lRLd0m3S7jl2qDUqiI",
    authDomain: "product-4e7a2.firebaseapp.com",
    databaseURL: "https://product-4e7a2-default-rtdb.firebaseio.com",
    projectId: "product-4e7a2",
    storageBucket: "product-4e7a2.appspot.com",
    messagingSenderId: "865728206867",
    appId: "1:865728206867:web:e0c40e9c2720d2069247e6"
};

var firebaseDatabase = firebase.initializeApp(firebaseConfig);

export default firebaseDatabase.database().ref();