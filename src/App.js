import React from 'react';
import Products from './Products';

function App() {
    return (
      <div className="row">
        <div className="col-md-8 offset-md-2">
          <Products/>
        </div>
      </div>
    );
  }
export default App;