import React, { useState, useEffect } from 'react';
import ProductForm from "./ProductForm";
import firebaseDatabase from "./firebase";

const Products = () => {

	var [currentId, setCurrentId] = useState('');
    var [productObjects, setProductObjects] = useState({})

    useEffect(() => {
        firebaseDatabase.child('product').on('value', snapshot => {
            if (snapshot.val() != null) {
                setProductObjects({
                    ...snapshot.val()
                });
            }
        })
    }, [])

    const addOrEdit = (obj) => {
        if (currentId === '')
            firebaseDatabase.child('product').push(
                obj,
                err => {
                    if (err)
                        console.log(err)
                    else
                        setCurrentId('')
                })
        else
            firebaseDatabase.child(`product/${currentId}`).set(
                obj,
                err => {
                    if (err)
                        console.log(err)
                    else
                        setCurrentId('')
                })
    }

    const onDelete = id => {
        firebaseDatabase.child(`product/${id}`).remove(
            err => {
                if (err)
                    console.log(err)
                else
                    setCurrentId('')
            })
    }

  return (
        <>
            <div className="jumbotron">
                <div className="container">
                    <h1 className="text-center">Агуулахын бүртгэл</h1>
                </div>
            </div>
            <div className="col-sm-8 text-center">
                <ProductForm {...({ currentId, productObjects, addOrEdit })} ></ProductForm>
            </div>
            <div>
                <table className="table table-borderless table-stripped">
                    <thead className="thead-light">
                        <tr>
                            <th>Бүтээгдхүүн</th>
                            <th>Тоо</th>
                            <th>Шинэчлэх/Устгах</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            Object.keys(productObjects).map((key) => (
                                <tr key={key}>
                                    <td>{productObjects[key].product}</td>
                                    <td>{productObjects[key].amount}</td>
                                    <td className="bg-light">
                                        <a className="btn text-primary" onClick={() => { setCurrentId(key) }}>
                                            <i className="fas fa-pencil-alt"></i>
                                        </a>
                                        <a className="btn text-danger" onClick={() => { onDelete(key) }}>
                                            <i className="far fa-trash-alt"></i>
                                        </a>
                                    </td>
                                </tr>
                            ))
                        }
                    </tbody>
                </table>
            </div>
        </>
    );
}

export default Products;